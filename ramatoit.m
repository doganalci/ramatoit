%ra=evalfis();

oyku;
anticpp;
sonuc;
rapor;

%kabuller.
%anticpp==1 pozitif.
%anticpp==0 negatif.
% sonuc=0 hasta de�il.
% sonuc=1 hasta.

%[0-40]
if ra<40&&ra>0 
    if oyku<25&&oyku>0 
        if anticpp==0 
            sonuc=0;
            rapor='Hasta RA hastas� de�ildir.';
            disp(rapor);
        else
            sonuc=0;
            rapor='Hasta RA hastas� de�ildir. Ancak erken evre Romatoid artrit hastal��� ge�irme ve ilerde Romatoid artrit geli�me ihtimali mevcuttur.  American College of Rheumatology�e g�re Anti-CCP antikorlar� pozitif olan ki�ilerde ileride % 95 olas�l�kla Romatoid Artrit geli�ecektir. ';
            disp(rapor);
        end
    end
    if oyku<50&&oyku>25 
        if anticpp==0 
            sonuc=0;
            rapor='Hasta RA hastas� de�ildir';
            disp(rapor);
        else
            sonuc=0;
            rapor='Hasta RA hastas� de�ildir. Ancak erken evre Romatoid artrit hastal��� ge�irme ve ilerde Romatoid artrit geli�me ihtimali mevcuttur.  American College of Rheumatology�e g�re Anti-CCP antikorlar� pozitif olan ki�ilerde ileride % 95 olas�l�kla Romatoid Artrit geli�ecektir. ';
            disp(rapor);
        end
    end
    if oyku<75&&oyku>50 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastad�r. Laboratuvar sonu�lar� normal g�z�kse de �yk� sonucuna g�re RA hastas� oldu�u g�r�lmektedir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastad�r. Laboratuvar sonu�lar� normal g�z�kse de �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir.  Ayr�ca Anti-CCP pozitifli�i de hastan�n RA hastas� oldu�unu destekler niteliktedir';
            disp(rapor);
        end
    end
    if oyku<100&&oyku>75 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastad�r. Laboratuvar sonu�lar� normal g�z�kse de �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastad�r. Laboratuvar sonu�lar� normal g�z�kse de �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir.  Ayr�ca Anti-CCP pozitifli�i de hastan�n RA hastas� oldu�unu destekler niteliktedir. ';
            disp(rapor);
        end
    end          
end

%[40-60]
if ra<60&&ra>40 
    if oyku<25&&oyku>0 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r';
            disp(rapor);
        end
    end
    if oyku<50&&oyku>25 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        end
    end
    if oyku<75&&oyku>50 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. Ayr�ca Anti-CCP ve RF pozitifli�i de hastan�n RA hastas� oldu�unu destekler niteliktedir. .';
            disp(rapor);
        end
    end
    if oyku<100&&oyku>75 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. Ayr�ca Anti-CCP ve RF pozitifli�i de hastan�n RA hastas� oldu�unu destekler niteliktedir. ';
            disp(rapor);
        end
    end          
end

%[60-80]
if ra<80&&ra>60 
    if oyku<25&&oyku>0 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r.';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        end
    end
    if oyku<50&&oyku>25 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        end
    end
    if oyku<75&&oyku>50 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir.  ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. Ayr�ca Anti-CCP ve RF pozitifli�i de hastan�n RA hastas� oldu�unu destekler niteliktedir.  ';
            disp(rapor);
        end
    end
    if oyku<100&&oyku>75 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. Ayr�ca Anti-CCP ve RF pozitifli�i spesifiklik a��s�ndan hastan�n RA hastas� oldu�unu destekler niteliktedir. ';
            disp(rapor);
        end
    end          
end

%[80-100]
if ra<80&&ra>100 
    if oyku<25&&oyku>0 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. Ayr�ca Anti-CCP ve RF pozitifli�i spesifiklik a��s�ndan hastan�n RA hastas� oldu�unu destekler niteliktedir. ';
            disp(rapor);
        end
    end
    if oyku<50&&oyku>25 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta RA hastas�d�r. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta RA hastas�d�r. Ayr�ca Anti-CCP ve RF pozitifli�i spesifiklik a��s�ndan hastan�n RA hastas� oldu�unu destekler niteliktedir.  ';
            disp(rapor);
        end
    end
    if oyku<75&&oyku>50 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta �iddetli RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta �iddetli RA hastas�d�r. �yk� sonucuna g�re hastan�n a�r�s� vard�r ve hastal���n aktivitesi y�ksektir.  Ayr�ca Anti-CCP ve RF pozitifli�i spesifiklik a��s�ndan hastan�n RA hastas� oldu�unu destekler niteliktedir.  ';
            disp(rapor);
        end
    end
    if oyku<100&&oyku>75 
        if anticpp==0 
            sonuc=1;
            rapor='Hasta �iddetli RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir. ';
            disp(rapor);
        else
            sonuc=1;
            rapor='Hasta �iddetli RA hastas�d�r. �yk� sonucuna g�re hastan�n �iddetli a�r�s� vard�r ve hastal���n aktivitesi y�ksektir.  Ayr�ca Anti-CCP ve RF pozitifli�i spesifiklik a��s�ndan hastan�n RA hastas� oldu�unu destekler niteliktedir. ';
            disp(rapor);
        end
    end          
end

